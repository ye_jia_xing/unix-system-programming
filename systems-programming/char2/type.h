/************* type.h file *****************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

# define N 10


typedef struct node NODE;
struct node {
    struct node *next;
    int id;
    char name[64];
};

int printlist(NODE *p);

int printlist(NODE *p)
{
    while (p)
    {
        printf("[%s %d]->", p->name, p->id);
        p = p->next;
    }
    printf("NULL\n");
}
/************* end of type.h file **********/