/******************* c2.2.c file ******************/
#include "type.h"

NODE *mylist, *node;    // pointers, no NODE area yet

int main()
{
    int i;
    NODE *p;
    node = (NODE *)malloc(N * sizeof(NODE));
    for (i = 0; i < N; i++) {
        p = &node[i];
        sprintf(p->name, "%s%d", "node", i);
        p->id = i;
        p->next = p+1;
    }
    node[N-1].next = 0;
    mylist = &node[0];
    printlist(mylist);
}