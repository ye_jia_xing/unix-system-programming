/****************** dlist Program c2.5.c **************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node NODE;
struct node {
    struct node *next;
    struct node *prev;
    int key;
};

NODE *dlist;        // dlist is a NONE pointer

int insert2end(NODE **list, int key) // insert to list END
{
    NODE *p, *q;
    // printf("insert2end: key=%d", key);
    p = (NODE *)malloc(sizeof(NODE));
    p->key = key;
    p->next = 0;
    q = *list;
    if (q == 0) {
        *list = p;
        p->next = p->prev = 0;
    } else {
        while (q->next) {
            q = q->next;
        }
        q->next = p;
        p->prev = q;
    }
}