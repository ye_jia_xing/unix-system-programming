/************** type.h file ***************/
#define NPROC  9       // numbers of PROCs
#define SSIZE 1024     // stack size = 4KB

// PROC status
#define FREE 0
#define READY 1
#define SLEEP 2
#define ZOMBIE 3

typedef struct proc PROC;
struct proc{
    struct proc *next;
    int *ksp;               // saved stack pointer
    int pid;                // pid = 0 to NPROC - 1
    int ppid;
    int status;
    int priority;
    int kstack[SSIZE];
};