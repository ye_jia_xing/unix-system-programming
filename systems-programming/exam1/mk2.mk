test1: main.o add.o sub.o mul.o div.o
	gcc -Iinclude lib/add.o lib/sub.o lib/mul.o lib/div.o bin/main.o -o bin/testmymath
add.o: src/add.c include/head.h
	gcc -Iinclude -c src/add.c -o lib/add.o
sub.o: src/sub.c include/head.h
	gcc -Iinclude -c src/sub.c -o lib/sub.o
mul.o: src/mul.c include/head.h
	gcc -Iinclude -c src/mul.c -o lib/mul.o
div.o: src/div.c include/head.h
	gcc -Iinclude -c src/div.c -o lib/div.o
main.o: src/main.c include/head.h 
	gcc -Iinclude -c src/main.c -o bin/main.o
all: test1

alib: add.o sub.o mul.o div.o
	ar rcs lib/libmymath.a lib/add.o lib/sub.o lib/mul.o lib/div.o
	gcc src/main.c -o bin/atest3 -I include/ -L ./lib -lmymath
solib: src/add.c src/sub.c src/mul.c src/div.c 
	gcc -fPIC -c src/add.c -Iinclude -o lib/soadd.o
	gcc -fPIC -c src/sub.c -Iinclude -o lib/sosub.o
	gcc -fPIC -c src/mul.c -Iinclude -o lib/somul.o
	gcc -fPIC -c src/div.c -Iinclude -o lib/sodiv.o
	gcc -shared -o lib/mymath.so lib/soadd.o lib/sosub.o lib/somul.o lib/sodiv.o
	gcc src/main.c ./lib/mymath.so -Iinclude -o bin/sotest4


