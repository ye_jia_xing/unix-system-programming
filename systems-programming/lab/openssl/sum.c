int sum(int N, int arr[])
{
	int s = 0;
	for (int i = 0; i < N; i++) {
		s += arr[i];
	}
	return s;
}
