A=xyz
echo \$A        ==> $A          # back quote $ as is
echo '$A'       ==> $A          # NO substitution within SINGLE quotes
echo "see $A"   ==> see xyz     # substitution $A in DOUBLE quotes
