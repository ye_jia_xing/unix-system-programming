
# ---------------- mk1 file --------------------------------
CC = gcc                # define CC as gcc
CFLAGS = -Wall          # define CLAGS as flags to gcc
OBJS = t.o mysum.o      # define Object code files
INCLUDE = -Ipath        # define path as an INCLUDE directory

all: mymath install        # build all listed targets: mymath, install

mymath: t.o mysum.o        # target: dependency list of .o files
	$(CC) $(CFLAGS) -o mymath $(OBJS) $(INCLUDE)

t.o: t.c type.h         # t.o depend on t.c and type.h
	gcc -c t.c
mysum.o: mysum.c type.h # mysum.o depend mysum.c and type.h
	gcc -c mysum.c

install: mymath            # depend on mymath: make will build mymath first
	echo install mymath to /usr/local/bin
	sudo mv mymath /usr/local/bin/     # install mymath to /usr/local/bin/

run: install            # depend on install, which depend on mymath
	echo run executable image mymath
	mymath || /bin/true    # no make error 10 if main() return non-zero

clean:
	rm -f *.o 2> /dev/null          # rm all *.o files
	sudo rm -f /usr/local/bin/mymath   # rm mymath
