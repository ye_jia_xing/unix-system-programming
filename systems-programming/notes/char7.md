# 第7章 文件操作
## 7.1 文件操作级别
文件操作分为五个级别：
（1）硬件级别：
+ fdisk：将硬件、U盘或SDC盘分区。
+ mkfs：格式化磁盘分区，为系统做好准备。
+ fsck：检查和维修系统。
+ 碎片整理：压缩文件系统中的文件。

（2）操作系统中的文件系统函数
（3）系统调用
（4）I/O库函数
（5）用户命令
（6）sh脚本
## 7.2 文件I/O操作
图7.1

用户模式下的程序执行操作
```
FILE *fp = fopen("file", "r");
```
or
```
FILE *fp = fopen("file", "w");
```
可以打开一个读/写文件流。

## 7.3 低级别文件操作
### 7.3.1 分区
一个块存储设备，如硬盘、U盘、SD卡等，可以分为几个逻辑单元，称为分区。各分区均可以格式化为特定的文件系统，也可以安装在不同的操作系统上。大多数引导程序，如GRUB、LILO等，都可以配置为从不同的分区引导不同的操作系统。分区表位于第一个扇区的字节偏移446（0x1BE）处，该扇区称为设备的主引导记录（MBR）。表有4个条目，每个条目由一个16字节的分区结构体定义，即：
```
struct partition {
    u8 drive;           // 0x80 - active
    u8 head;            // starting head
    u8 sector;          // starting sector
    u8 cylinder;        // starting cylinder
    u8 sys_type;        // partition type
    u8 end_head;        // end head
    u8 end_sector;      // end sector
    u8 end_cylinder;    // end cylinder
    u32 start_sector;   // starting sector counting from  0
    u32 nr_sectors;     // number of sectors in partition
};
```
如果某分区是扩展类型（类型编号=5），那么它可以划分为更多分区。假设分区P4是扩展类型，它被划分为扩展分区P5、P6、P7。扩展分区在扩展分区区域内形成一个链表，如图7.2所示。

每个扩展分区的第一个扇区是一个**本地MBR**。


### 7.3.2 格式化分区
fdisk只将一个存储设备划分为多个分区。每个分区都有特定的文件系统类型，但是分区还不能使用。为了存储文件，必须先为特定的文件系统准备好分区。该操作习惯上称为**格式化**磁盘或磁盘分区。在Linux中，它被称为mkfs，表示Make文件系统。Linux支持多种不同类型的文件系统。每个文件系统都期望存储设备上有特定的格式。在Linux中，命令
```
mkfs -t TYPE [-b bsize] device nblocks
```
在一个nblocks设备上创建一个TYPE文件系统，每个块都是bsize字节。如果bsize未指定，则默认块大小为1KB。具体来说，假设是EXT2/3文件系统，它是Linux的默认文件系统。因此，
```
mkfs -t ext2 vdisk 1440
```
或
```
mke2fs vdisk 1440
```
使用1440（1KB）个块将vdisk格式化为EXT2文件系统。格式化后的磁盘应是只包含根目录的空文件系统。但是，Linux的mkfs始终会在根目录下创建一个默认的lost+found目录。
/mnt目录通常用于挂载其他文件系统。
### 7.4.2 超级块
**Block#1：超级块**（在硬盘分区中字节偏移量为1024）B1是超级块，用于容纳关于整个文件系统的信息。下文说明了超级块结构中的一些重要字段。
```
struct ext2_super_block {
    u32 s_inodes_count;         // Inodes count
    u32 s_blocks_count;         // Blocks count
    u32 s_r_blocks_count;       // Reserved blocks count
    u32 s_free_blocks_count;    // Free blocks count
    u32 s_free_inodes_count;    // Free inodes count
    u32 s_first_data_block;     // First Data Block
    u32 s_log_block_size;       // Block size
    u32 s_log_cluster_size;     // Allocation cluster size
    u32 s_blocks_per_group;     // # Blocks per group
    u32 s_clusters_per_group;   // # Fragments per group
    u32 s_inodes_per_group;     // # Inodes per group
    u32 s_mtime;                // Mount time
    u32 s_wtime;                // Write time
    u32 s_mnt_count;            // Mount count
    u16 s_max_mnt_count;        // Maximal mount count
    u16 s_magic;                // Magic signature
    // more non-essential fields
    u16 s_inode_size;           // size of inode structure
};
```

### 7.4.3 块组描述符
**Block#2：块组描述符块**（硬盘上的s_first_data_blocks-1）EXT2将磁盘块分成几个组。每个组有8192个块（硬盘上的大小为32K）。每组用一个块组描述符结构体描述。

### 7.4.4 位图
**Block#8：块位图**（Bmap）（bg_block_bitmap）位图用来表示某种项的位序列，例如，磁盘块或索引节点。位图用于分配和回收项。
在位图中，0位表示对应项处于FREE状态，1位表示对应项处于IN_USE状态。一个软盘有1440块。
**Block#9：索引节点位图**（Imap）（bg_inode_bitmap）一个**索引节点**就是用来代表一个文件的数据结构。EXT2文件系统是使用有限数量的索引节点创建的。各索引节点的状态用B9中Imap中的一个位表示。在EXT2 FS中，前10个索引节点是预留的。

### 7.4.5 索引节点
**Block#10：索引（开始）节点块**（bg_inode_table）每个文件都用一个128字节（EXT4中的是256字节）的独特索引节点结构体表示。

### 7.4.6 目录条目
**EXT2目录条目；**目录包含dir_entry_2结构，即：
```
struct ext2_dir_entry_2 {
    u32 inode;
    u16 rec_len;
    u8 name_len;
    u8 file_type;
    char name[EXT2_NAME_LEN];
};
```