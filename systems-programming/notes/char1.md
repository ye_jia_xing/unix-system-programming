# 第一章 引言

## 1.1 关于本书

## 1.2 系统编程的作用

## 1.3 本书的目标

### 1.3.1 强化学生的编程背景知识

本书的第一个目标是为学生提供高级编程所需的背景知识和技能。书中详细介绍了程序开发步骤，包括汇编器、编译器、链接器、链接库、可执行文件内容、程序执行映像、函数调用约定、参数传递方案、局部变量、栈帧、将C程序与汇编代码链接、程序终止和异常处理。此外，还说明了如何使用makefile管理大型编程项目，以及如何使用GDB调试程序的执行。

 ### 1.3.2 动态数据结构的作用

大多数书很少说明动态数据结构在实践中的用处和使用方式。

### 1.3.3 进程概念和进程管理

系统编程的重点是各种进程的抽象概念。

### 1.3.4 并发编程

并行计算代表着计算的未来。

### 1.3.5 定时器和定时功能

### 1.3.6 信号、信号处理和进程间通信

信号和信号处理是理解程序异常和进程间通信(IPC)的关键。

### 1.3.7 文件系统

除进程管理之外，文件系统是操作系统的第二大组成部分。

### 1.3.8 TCP/IP和网络编程

网络编程是操作系统的第三大组成部分。本书介绍了TCP/IP协议、套接字API、UDP和TCP套接字编程，以及网络计算中的服务器-客户机模型。

## 1.4 目标读者

## 1.5 本书的独特之处

（1）本书自成一体。

（2）本书使用一个简单的多任务系统来介绍和阐释了进程的抽象概念。

（3）本书详细简述了Unix/Linux中各种进程的来源和作用，包括系统启动、初始化进程、守护进程、登录进程以及用户sh进程等。

（4）本书详细描述了Unix/Linux中的各种进程管理函数，包括fork()、exit()、wait()、通过exec()更改进程执行映像、I/O重定向和管道。

## 1.6 将本书用作系统编程课程的教材

## 1.7 其他参考书

## 1.8 关于Unix

Unix是一种通用操作系统。

开发者：肯·汤普森（Ken Thompson）和丹尼斯·里奇（Dennis Richie）

诞生日期：20世纪70年代早期

经典书目：K&R 1988 《The C Programming Language》

### 1.8.1 AT&T Unix

### 1.8.2 Berkeley Unix

### 1.8.3 HP Unix

### 1.8.4 IBM Unix

### 1.8.5 Sun Unix

## 1.9 关于Linux

```
Linux（Linux 2017）是一个类Unix系统。它最初是Linus Torvalds在1991年为基于Intel x86的个人计算机开发的一个实验性内核。

Linux的一个重要里程碑发生在20世纪90年代末，当时，它与GNU（Stallman 2017）相结合，纳入了许多GNU软件，如GCC编译器、GNU emacs编辑器和bash等，极大地促进了Linux的进一步发展。不久之后，Linux实现了访问互联网的TCP/IP协议族，并移植了支持GUI的X11（X-window），成为一个完整的操作系统。

Linux包含其他Unix系统的许多特性。在某种意义上，它是由各种最为流行的Unix系统组合而成。在很大程度上，Linux是兼容POSIX标准的。Linux已被移植到许多硬件体系结构中，如摩托罗拉、SPARC和ARM等。主要的Linux平台仍然是基于Intel x86的个人计算机，包括广泛可用的台式机和笔记本电脑。此外，Linux可免费使用，且易于安装，因此，颇受计算机科学专业的学生欢迎。
```

## 1.10 Linux版本

```
Linux内核的开发是在Linux内核开发小组的严格监督下进行的。除不同的发行版之外，所有Linux内核都完全相同。但根据发行版的不同，Linux有许多不同的版本，这些版本可能在发行版软件包、用户界面和服务功能上有所不同。
```

### 1.10.1 Debian Linux

```
Debian（得比安）是专注于免费软件的Linux发行版。可支持很多软件平台。Debian发行版采用.deb包格式和dpkg包管理器及其前端。
```

### 1.10.2 Ubuntu Linux

```
Ubuntu是基于Debian的Linux发行版。
```

### 1.10.3 Linux Mint

```
Linux Mint是基于Debian和Ubuntu的社区主导型Linux发行版。

致力于构建一个“既强大又易于使用的现代、优雅和舒适的操作系统”。

Linux Mint通过纳入一些专有软件来提供完全开箱即用的多媒体支持，并与各种免费和开源应用程序绑定在一起。因此，它受到了许多Linux初学者的欢迎。
```

### 1.10.4 基于RPM的Linux

```
Red Hat Linux和SUSE Linux是最早使用RPM文件格式的主要发行版，目前仍有一些软件包管理系统在使用这种格式。这两种发行版都分为商业和社区支持版本。例如，Red Hat Linux提供了一个由Red Hat赞助的社区支持发行版（称为Fedora）和一个商业支持发行版（称为Red Hat Enterprise Linux），而SUSE分为openSUSE和SUSE Linux Enterprise。
```

### 1.10.5 Slackware Linux

```
Slackware Linux发行版以高度可定制而著称，专注于通过尖端的软件和自动化工具来提供维护便捷性和可靠性。该发行版适合Linux高级用户使用。它允许用户选择Linux系统组件来安装系统和配置已安装的系统，使其能够了解Linux操作系统的内部工作原理。
```

## 1.11 Linux硬件平台

```
Linux最初是为基于Intel x86的个人计算机而设计。早期的Linux版本是以32位保护模式在基于Intel x86的个人计算机上运行。现在，它可同时在32位和64位模式下使用。除Intel x86之外，Linux已移植到许多其他的计算机体系结构上，包括摩托罗拉MC6800、MIP、SPARC、PowerPC以及最近的ARM。但Linux的主要硬件平台仍然是基于Intel x86的个人计算机，尽管基于ARM的Linux嵌入式系统正在迅速普及。
```

## 1.12 虚拟机上的Linux

```
目前，大多数基于Intel x86的个人计算机都以Microsoft Windows为默认操作系统，例如，Windows7、8或10。在同一台个人计算机上安装Linux和Windows相当容易，并且可在开机时使用双启动来启动Windows或Linux。然而，大多数用户不愿这样做，原因要么存在技术上的困难，要么是更偏向于Windows环境。通常的做法是在Windows主机内的虚拟机上安装和运行Linux。
```

### 1.12.1 VirtualBox

### 1.12.2 VMware

### 1.12.3 双启动Slackware和Ubuntu Linux

## 1.13 使用Linux

<br/>

### 1.13.1 Linux内核镜像

  在典型的Linux系统中，Linux内核映像位于/boot目录中。可启动的Linux内核映像名为vmlinuz-generic-VERSION_NUMBER
initrd是Linux内核的初始的ramdisk映像。
    一个可启动的Linux内核映像由三部分组成：
    | BOOT | SETUP | linux kernel
    BOOT是一个512字节的启动程序，用于从软盘映像启动早期的Linux版本。但现在，它不再用于Linux启动，而是包含一些参数，以供“SETUP”使用。SETUP是一段16位和32位的汇编代码，用于在启动期间将16位模式转换为32位保护模式。linux kernel是Linux的实际内核映像。它采用压缩格式，但在开始时有一段解压代码，用于解压Linux内核映像并将其放入高端内存中。

### 1.13.2 Linux启动程序

```
Linux内核可以由几个不同的启动加载程序启动。最受欢迎的Linux启动加载程序是GRUB和LILO。另外，HD启动程序（Wang 2015）也可用来启动Linux。
```

### 1.13.3 Linux启动

在启动期间，Linux启动加载程序首先会定位Linux内核映像（文件）。然后：
+ 加载BOOT+SETUP至实模式内存的0x90000处。
+ 加载Linux内核至高端内存的1MB处。

对于常见的Linux内核映像，该加载程序还会将初始ramdisk映像initrd加载到高端内存中。然后转移控制权，运行0x902000处的SETUP代码，启动Linux内核。首次启动时，Linux内核会将initrd作为临时根文件系统，在initrd上运行。Linux内核将执行一个sh脚本，该脚本指示内核加载实际根设备所需的模块。当实际根设备被激活并准备就绪时，内核将放弃初始ramdisk，将实际根设备挂载为根文件系统，从而完成Linux内核的两阶段启动。



### 1.13.4 Linux运行级别

Linux内核以单用户模式启动。它可以模仿System V Unix的运行级别，以多用户模式运行。然后，创建并运行INIT进程P1，后者将创建各种守护进程和供用户登录的终端进程。之后，INIT进程将等待任何子进程终止。

### 1.13.5 登录进程
各登录进程将在其终端上打开三个文件流：stdin（用于输入），stdout（用于输出），stderr（用于错误输出）。然后等待用户登录。在使用X-window作为用户界面的Linux系统上，X-window服务器通常会充当用户登录界面。用户登录后，（伪）终端默认属于用户。
### 1.13.6 命令执行
登录后，用户进程通常会执行命令解释程序sh，后者将提示用户执行命令。sh将直接执行一些特殊命令，如cd（更改目录）、exit（退出）、logout（注销）、&。

## 1.14 使用Ubuntu Linux

### 1.14.1 Ubuntu版本

### 1.14.2 Ubuntu Linux的特性
（1）在台式机或笔记本电脑上安装Ubuntu时，需要输入用户名和密码来创建一个默认主目录为“/home/username”的用户账户。当Ubuntu启动时，它会立即在用户环境中运行，因为其已自动登录默认用户。

（2）出于安全原因，用户应为普通用户，而不是根用户或超级用户。
要运行任何特权命令，用户必须输入

`sudo command`

（3）用户的“PATH”（路径）环境变量设置通常不包括用户的当前目录。为在当前目录下运行程序，用户每次必须输入./a.out。为方便起见，用户应更改路径设置，以包含当前目录。在用户的主目录中，创建一个包含以下代码的.bashrc文件：

`PATH=$PATH:./`

用户每次打开伪终端时，sh都会先执行.bashrc文件来设置路径，以包含当前工作目录。

（4）很多用户可能都安装了64位的Ubuntu Linux。本书中的一些编程练习和作业是针对32位机器的。在64位Linux下，使用
`gcc -m32 t.c # compile t.c into 32-bit code`

生成32位代码。若64位Linux不采用`-m32`选项，则用户必须安装适用于gcc的附加支持插件，才能生成32位代码。

（5）Ubuntu具有友好的GUI用户界面。许多用户都习惯于使用GUI，以至于产生了过度依赖，这往往需要反复拖动和点击指向设备，从而浪费了大量时间。在系统编程中，用户还需要学习如何使用命令行和sh脚本，它们比GUI要通用和强大得多。
## 1.15 Unix/Linux文件系统组织
Unix/Linux文件系统采用树形组织结构。

Unix/Linux将所有能够存储
### 1.15.1 文件类型
（1）**目录文件**：一个目录可能包含其他目录和（非目录）文件。

（2）**非目录文件**：非目录文件要么是“REGULAR”（常规）文件，要么是“SPECIAL”（特殊）文件，但只能是文件系统树中的叶节点。非目录文件可进一步分为：
+ **常规文件**：常规文件也称为“ORDINARY”（普通）文件。这些文件要么包含普通文本，要么包含可执行的二进制代码。
+ **特殊文件**：特殊文件是`/dev`目录中的条目。它们表示I/O设备，可进一步分为：
  + **字符特殊文件**：字符I/O，如/dev/tty0、/dev/pts/l等。
  + **块特殊文件**：块I/O，如/dev/had、/dev/sda等。
  + 其他类型，如网络（套接字）特殊文件、命名管道等。

（3）**符号链接文件**：属于常规文件，其内容为其他文件的路径名。因此，这些文件是指向其他文件的指针。例如，Linux命令
`ln -s aVeryLongFileName myLink`

可创建一个符号链接文件“mylink”，其指向“aVeryLongFileName”。对“mylink”的访问将被重定向到实际文件“aVeryLongFileName”上。
### 1.15.2 文件路径名

Unix/Linux文件系统树的根节点（用“/”符号表示）称为**根目录**，或简称为根。文件系统树的每个节点都由以下表单的路径名指定：

  `/a/b/c/d`  OR  `a/b/c/d`
以“/”开头的路径名为绝对路径名，反之则为相对于进程当前工作目录（CWD）的相对路径名。当用户登录到Unix/Linux时，CWD即被设为用户的主目录。CWD可通过cd（更改目录）命令更改。pwd命令可打印CWD的绝对路径名。
### 1.15.3 Unix/Linux命令
Unix/Linux中最常用的命令：
+ ls：ls dirname：列出CWD或目录的内容。
+ cd dirname：更改目录。
+ pwd：打印CWD的绝对路径名。
+ touch filename：更改文件名时间戳（如果文件不存在，则创建文件）。
+ cat filename：显示文件内容。
+ cp src dest：复制文件
+ mv src dest：移动或重命名文件。
+ mkdir dirname：创建目录。
+ rmdir dirname：移除（空）目录。
+ rm filename：移除或删除文件。
+ ln oldfile newfile：在文件之间创建链接。
+ find：搜索文件。
+ grep：搜索文件中包含模式的行。
+ ssh：登录到远程主机。
+ gzip filename：将文件压缩为.gz文件。
+ tar -zcvf file.tgz.:从当前目录创建压缩tar文件。
+ tar -zxvf file.tgz.:从.tgz文件中解压文件。
+ man：显示在线手册页。
+ zip file.zip filenames：将文件压缩为.zip文件。
+ unzip file.zip：解压.zip文件。

### 1.15.4 Linux手册页
Linux将在线手册页保存在标准/usr/man目录下。在Ubuntu Linux中，手册页保存在/usr/share/man目录下。手册页分为不同类别，用man1、man2等表示。

  `/usr/man`</br>
    |-- man1：常用命令：ls、cat、mkdir等</br>
    |-- man2：系统调用</br>
    |-- man3：库函数：strtok、strcat、basename、dirname等
所有手册页均为压缩.gz文件。其中包含描述性文本，说明了如何使用附有输入参数和选项的命令。man是一个程序，可读取手册页并以用户友好的格式显示其内容。下面是使用手册页的一些例子。
+ man ls:显示man1中ls命令的手册页。
+ man 2 open：显示man2中open函数的手册页。
+ man strtok：显示man3中strtok函数的手册页等。
+ man 3 dirname：显示man3（而非man1）中dirname函数。
如有需要，应查看这些手册页，了解如何使用特定的Linux命令。很多所谓的Unix/Linux系统编程书籍实际上都是Unix/Linux手册页的浓缩版。
## 1.16 Ubuntu Linux系统管理
### 1.16.1 用户账户
与在Linux中一样，用户账户信息保存在/etc/passwd文件中，该文件归超级用户所有，但任何人都可以读取。在表单的/etc/passwd文件中，每个用户都有一个对应的记录行：</br>
`loginName:x:gid:uid:usefInfo:homeDir:initalProgram`
其中第二个字段“x”表示检查用户密码。加密的用户密码保存在`/etc/shadow`文件中。shadow文件的每一行都包含加密的用户密码，后面是可选的过期限制信息，如过期日期和时间等。当用户尝试使用登录名和密码登录时，Linux将检查/etc/passwd文件和/etc/shadow文件，以验证用户的身份。用户成功登录后，登录进程将通过获取用户的gid和uid来转换成用户进程，并将目录更改为用户的homeDir，然后执行列出的initialProgram，该程序通常为命令解释程序sh。
### 1.16.2 添加新用户
我们可以假设：用户想添加一名家庭成员，以便使用同一台计算机。与在Linux中一样，Ubuntu支持“adduser”（添加用户）命令，运行方式如下：

  `sudo adduser` username
它通过为新用户创建账户和默认的主目录/home/username来添加新用户。此后，Ubuntu将在“About The Computer”（关于本电脑）菜单中显示用户名列表。新用户可通过选择新用户名来登录系统。
### 1.16.3 sudo命令
出于安全原因，Ubuntu禁用了**根**或**超级用户**账户，这可防止任何人以根用户身份登录（其实也不完全是，有一种方法可以，但不方便透露）。sudo（“超级用户执行”）允许用户提升到超级用户特权级别。完成命令执行后，用户进程将恢复到原来的特权级别。为确保能够使用sudo，用户名必须保存在/etc/sudoers文件中。为确保用户能够发出sudo，只需在sudoers文件中添加一行，如下所示：
`username ALL(ALL)  ALL`
但/etc/sudoers文件的格式非常严格。文件中的任何语法错误都可能破坏系统安全性。Linux建议只使用特殊命令visudo来编辑该文件，该命令可调用vi编辑器，但需要检查和验证。