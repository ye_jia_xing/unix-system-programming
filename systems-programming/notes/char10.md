# 第10章 sh编程

## 10.1 sh脚本
sh脚本是一个包含sh语句的文本文件，命令解释程序sh要执行该语句。例如，我们可以创建一个文本文件，命令解释程序sh要执行该语句。例如，我们创建文本文件mysh：
```
#! /bin/bash
# comment line
echo hello
```
使用`chmod +x mysh`使其可执行。然后运行mysh。sh脚本的第一行通常以#!组合开始，通常称为shebang。当sh见到shebang时，会读取脚本所针对的程序名并调用该程序。sh有许多不同的版本，例如：Linux的bash、BSD Unix的csh和IBM AIX的ksh等。所有sh程序基本上都执行相同的任务，但它们的脚本在语法上略有不同。shebang允许主sh调用适当版本的sh来执行脚本。如果未指定shebang，它将运行默认的sh，即Linux中的/bin/bash。当bash执行mysh脚本时，将会打印hello。

## 10.2 sh脚本与C程序
sh脚本和C程序有些相似之处，但它们在根本上是不同的。

首先，sh是一个解释程序，逐行读取sh脚本文件并直接执行这些行。如果行是可执行命令且为内置命令，那么sh可直接执行。否则，它会复刻一个子进程来执行命令，并等待子进程终止后再继续，这与它执行单个命令行完全一样。相反，C程序必须先编译链接到一个二进制可执行文件，然后通过主sh的子进程运行二进制可执行文件。其次，在C程序中，每个变量必须有一个类型，例如char、int、float、派生类型（如struct）等。相反，在sh脚本中，每个变量都是字符串。因此不需要类型，因为只有一种类型，即字符串。最后，每个C程序必须有一个main()函数，每个函数必须定义一个返回值类型和参数。相反，sh脚本不需要main函数。在sh脚本中，第一个可执行语句是程序的入口点。

## 10.3 命令行参数
可使用与运行sh命令完全相同的参数调用sh脚本，如：
```
mysh one two three
```
在sh脚本中，可以通过位置参数$0, $1, $2等访问命令行参数。前10个命令行参数可以作为$0~$9被访问。其他参数必须称为${10}~${n}，其中n>10。或者，可以通过稍后显示的shift命令查看它们。通常，$0是程序名本身，$1到$n是程序的参数。在sh中，可用内置变量$#和$*计数并显示命令行参数。
$# = 命令行参数$1到$n的数量
$* = 所有命令行参数，包括$0
此外，sh还有与命令执行相关的以下内置变量。
$S = 执行sh的进程PID
$? = 最后一个命令执行的退出状态（如果成功，则为0，否则为非0）

## 10.4 sh变量
sh有许多内置变量，如PATH、HOME、Term等。除了内置变量外，用户还可以使用任何符号作为sh变量。不需要声明。所有的sh变量值都是字符串。未赋值的sh变量是NULL字符串。sh变量可用以下方法设置或赋值：
```
variable=string     # NOTE: on white spaces allowed between tokens
```
如果A是一个变量，则$A是变量的值。
示例10.2：
```
echo A      ==>     A
echo $A     ==>     (null if variable is not set)
A="this is fun"     # set A value
echo $A     ==>     this is fun
B = A               # assign "A" to B
echo $B     ==>     A(B was assigned the string "A")
B = $A              (B takes the VALUE of A)
echo $B     ==>     this is fun
```

## 10.5 sh中的引号
sh有许多特殊字符，如$、/、*、>、<等。要想把它们用作普通字符，可使用\或单引号来引用它们。
**示例10.3**
```
A=xyz
echo \$A        ==> $A          # back quote $ as is
echo '$A'       ==> $A          # NO substitution within SINGLE quotes
echo "see $A"   ==> see xyz     # substitution $A in DOUBLE quotes
```
通常，\用于引用单个字符。单引号用于引用长字符串。单引号内没有替换。双引号用于保留双引号字符串中的空格，但在双引号内会发生替换。

## 10.6 sh语句
sh语句包括所有Unix/Linux命令，以及可能的I/O重定向。
**示例10.4**
```
ls
ls > outfile
date
cp f1 f2
mkdir newdir
cat < filename
```
此外，sh编程语言还支持控制sh程序执行的测试条件、循环、case等语句
## 10.7 sh命令
### 10.7.1 内置命令
sh有许多内置命令，这些命令由sh执行，不需要创建一个新进程。下面列出一些常用的内置sh命令。
+ .file：读取并执行文件。
+ break[n]：从最近的第n个嵌套循环中退出。
+ cd[dirname]：更换目录。
+ continue[n]：重启最近的第n个嵌套循环。
+ eval[arg...]；计算一次参数并让sh执行生成的命令。
+ exec[arg...]：通过这个sh执行命令，sh将会退出。
+ exit[n]：是sh退出，退出状态为n。
+ export[var...]：将变量导出到随后执行的命令。
+ read[var...]：从stdin中读取一行并为变量赋值。
+ set[arg...]：在执行环境中设置变量。
+ shift：将位置参数$2$3...重命名为$1$2...。
+ trap[arg][n]：接收到信号n后执行参数。
+ umask[ddd]：将掩码设置为八进制数ddd的。
+ wait[pid]：等待进程pid，如果没有给出pid，则等待所有活动子进程。
**read命令：**当sh执行read命令时，它会等待来自stdin的输入行。它将输入行划分为几个标记，分配给列出的变量。read的一个常见用法是允许用户与正在执行的sh进行交互，如下面的示例所示：
```
echo -n "enter yes or no : " # wait for user input file from stdin
read ANS
echo $ANS
```
在获得输入后，sh可能会测试输入字符串，以决定下一步做什么。

### 10.7.2 Linux命令
sh可以执行所有的Linux命令。其中，有些命令几乎已经成为sh不可分割的一部分，因为它们广泛用于sh脚本中。下文列出并解释了其中一些命令。
**echo命令：**echo只是将参数字符串作为行回显到stdout。它通常将相邻的多个空格压缩为一个空格，除非有引号。