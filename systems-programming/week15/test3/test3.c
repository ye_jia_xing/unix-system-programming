#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define random_2(a,b) ((rand()%(b-a+1))+a)

void print_arr(int a[], int len) {
	for (int i = 0; i < len; i++) {
		printf("%d ", a[i]);
	}
	printf("\n");
}

int cmp(const void *a, const void *b)
{
    return *(int*)a - *(int*)b; //由小到大排序
    //return *(int *)b - *(int *)a; 由大到小排序
}

int main() {
	int arr[11];
    srand((int)time(NULL));
	arr[10] = 326;
	for (int i = 0; i < 10; i++) {
		arr[i] = random_2(1,1000);
	}
	print_arr(arr, 11);
	qsort(arr, 11, sizeof(int), cmp);
	print_arr(arr, 11);
	// search
	int *p;
	int key = 326;
	p = (int *)bsearch(&key, arr, 11, sizeof(int), cmp);
	(p == NULL) ? puts("not found") : puts("found");
	return 0;
}
