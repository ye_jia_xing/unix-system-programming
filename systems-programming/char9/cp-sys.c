#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

#define BLKSIZE 4096

int fd, gd;
char buf[4096];

int main(int argc, char const *argv[])
{
    int n, total = 0;
    if (argc < 3) exit(1);
    // check for same file
    fd = open(argv[1], O_RDONLY);
    if (fd < 0) exit(2);
    gd = open(argv[2], O_WRONLY|O_CREAT);
    if (gd < 0) exit(3);
    while (n = read(fd, buf, BLKSIZE))
    {
        write(gd, buf, n);
        total += n;
    }
    printf("total=%d\n", total);
    close(fd); close(gd);
    return 0;
}
