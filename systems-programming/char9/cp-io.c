#include <stdio.h>
#include <stdlib.h>
#define BLKSIZE 4096
FILE *fp, *gp;
char buf[4096];

int main(int argc, char const *argv[])
{
    int n, total = 0;
    if (argc < 3) exit(1);
    // check for same file
    fp = fopen(argv[1], "r");
    if (fp == NULL) exit(2);
    gp = fopen(argv[2], "w");
    if (gp == NULL) exit(3);
    while (n = fread(buf, 1, BLKSIZE, fp))
    {
        fwrite(buf, 1, n, gp);
        total += n;
    }
    printf("total = %d\n", total);
    fclose(fp); fclose(gp);
    return 0;
}
