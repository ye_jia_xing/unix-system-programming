#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char*argv[]) {
    int fd;
    int i, n;
    char buf[4096];
    if (argc < 2) exit(1);
    fd = open(argv[1], O_RDONLY);
    if (fd < 0) exit(2);
    while (n = read(fd, buf, 4096)) {
        for (i = 0; i < n; i++) {
            write(1, &buf[i], 1);
        }
    }
    
}