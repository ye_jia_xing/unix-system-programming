#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[]) {
    FILE *fp;
    
    int c;
    if (argc <  2) exit(1);
    fp = fopen(argv[1], "r");
    if (fp == 0) exit(2);
    while ((c = fgetc(fp)) != EOF)
    {
        putchar(c);
    }
    
    return 0;
}
