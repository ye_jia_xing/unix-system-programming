// file copy using getc(), putc()
#include <stdio.h>

FILE *fp, *gp;

int main(int argc, char const *argv[])
{
    int c; // for testing EOF
    fp = fopen("txt1.txt", "r"); // source
    gp = fopen("txt2.txt", "w"); // target
    while ((c = getc(fp)) != EOF)
    {
        putc(c, gp);
    }
    fclose(fp); fclose(gp);
    return 0;
}
